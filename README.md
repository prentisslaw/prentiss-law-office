Prentiss Law Office is the premier criminal defense office in Sacramento. This law firm focuses on the most serious felony cases and DUI's. For almost 15 years Timothy M. Prentiss II has been representing DUI clients in Redding CA, Sacramento and all of Northern CA. Let him fight your charges!

Address: 1020 12th St, #225, Sacramento, CA 95814, USA

Phone: 916-426-6804

Website: https://www.tprentisslaw.com/